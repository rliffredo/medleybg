# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import Image
import ImageOps

def anti_alias_rotate(im, angle, factor=4):
    """
    Rotate an image, using anti alias
    @param im: Image to rotate
    @param angle: rotation
    @param factor: antialias factor, should be multiple of 2. Higher values
        mean better antialiasing, but higher memory consumption and more
        processor time to perform the rotation
    @return: rotated Image
    """
    im2 = im.resize([s * factor for s in im.size], Image.ANTIALIAS)
    im2r = im2.rotate(angle, Image.BICUBIC, True)
    imf = im2r.resize([s // factor for s in im2r.size], Image.ANTIALIAS)
    return imf

def exif_rotate(im):
    """
    Rotate image if necessary, according to EXIF data
    @param im: Image to rotate
    @return: rotated image (if necessary)
    """
    try:
        orientation = im._getexif()[274] # See ExifTags.TAGS for tag names
    except (AttributeError, KeyError, TypeError):
        # No exif information - let's return the same image
        print 'no exif!'
        return im
    rotation = {1: 0, 2: 0, 3: 180, 4: 180,
                5:-90, 6:-90, 7: 90, 8: 90}
    reflection = {1: False, 2: True, 3: False, 4: True,
                  5: True, 6: False, 7: True, 8: False}
    if reflection[orientation]:
        im = ImageOps.mirror(im)
    if rotation[orientation]:
        im = im.rotate(rotation[orientation], Image.NEAREST, True)
    return im

def resize_fit(im, short_side_lenght):
    """
    resize image if necessary - maximum length of short size: 150px
    """
    if im.size[0] < im.size[1]:
        size = (short_side_lenght,
                int(1.0 * im.size[1] / im.size[0] * short_side_lenght))
    else:
        size = (int(1.0 * im.size[0] / im.size[1] * short_side_lenght),
                short_side_lenght)
    im = im.resize(size, Image.ANTIALIAS)
    return im
