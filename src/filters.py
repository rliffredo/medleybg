# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import ImageOps
import ImageEnhance
import ImageDraw

def desaturate(img):
    """
    @return a copy of the image, lighter and desaturated useful for background.
    """
    bw_img = ImageOps.grayscale(img)
    enhancer = ImageEnhance.Brightness(bw_img)
    bw_img_light = enhancer.enhance(0.50)
    bw_img_light = bw_img_light.convert('RGB')
    return bw_img_light

def addborder(img, external):
    """
    @return: image with a white border, like printed photos
    @param external: tells if border should be added to the picture, or should
                                     overwrite the outermost part
    """
    if external:
        img_to_expand = img
    else:
        img_to_expand = ImageOps.crop(img, border=10)
    im2 = ImageOps.expand(img_to_expand, border=10, fill="white")
    return im2

def putnumber(img, n):
    """
    @return image with a number printed on it. Useful for debugging purposes.
    @param n: number to write on the image
    """
    img = img.copy()
    draw = ImageDraw.Draw(img)
    draw.text((10, 10), str(n), fill='fuchsia')
    return img
