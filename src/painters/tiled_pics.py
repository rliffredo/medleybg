# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import math

import Image
import ImageOps

from util_m import paste_image, perform_loop


def _get_amt(tilesize, picsize):
    """
    Calculate the amount of pictures will be used to cover picsize, using
    square tiles with side tilesize
    """
    tile_x = tilesize[0]
    tile_y = tilesize[1]
    pic_x = picsize[0]
    pic_y = picsize[1]

    amt_x = math.ceil(pic_x * 1.0 / tile_x)
    amt_y = math.ceil(pic_y * 1.0 / tile_y)

    return int(amt_x * amt_y)


def _get_coords(i, tilesize, picsize):
    """
    Calculate the coordinates of a tile, given its number.
    Note: coordinates are relative to the area
    """
    tile_x = tilesize[0]
    tile_y = tilesize[1]
    pic_x = picsize[0]

    tiles_x = int(math.ceil(pic_x * 1.0 / tile_x))
    internal_pic_x = tiles_x * tile_x
    mat_x = ((tile_x * i) % internal_pic_x) / tile_x
    mat_y = (tile_x * i) // internal_pic_x
    x = mat_x * tile_x
    y = mat_y * tile_y

    return x, y

def tiled_pics(pic_stream, target_image, tgt_topleft, tgt_size, tilesize,
               debug=False):
    """
    Cover an area with squares from input pic_stream
    @param tgt_topleft: point from wich to start filling (top, left)
    @param tgt_size: size (h, v) to fill starting from tgt_topleft
    @param tilesize: size of the tile. Pics will be cropped/resized to fit in it
                     (width, height) 
    @param debug: if True, pictures will be marked with their positional number
    """
    def paint_picture(i):
        try:
            left, top = _get_coords(i, tilesize, tgt_size)
            left += tgt_topleft[1]
            top += tgt_topleft[0]
            im = pic_stream.get()
            if im.size != tilesize:
                im = ImageOps.fit(im, tilesize, Image.ANTIALIAS, 0, (0.5, 0.5))
            if debug:
                try:
                    import filters
                    im = filters.putnumber(im, i)
                except:
                    pass
            return paste_image(target_image, im, left, top, None)
        except:
            raise

    amt = _get_amt(tilesize, tgt_size)
    if debug:
        print "Tiling %s pictures on total area %s" % (amt, str(tgt_size))
    perform_loop(paint_picture, amt)
    return amt
