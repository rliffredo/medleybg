# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import threading
import Queue

WORKERS = 2

def _worker():
    while True:
        painter, pic_number = _worker.jobs.get()
        try:
            while True: # Repeat until paint is successful 
                if painter(pic_number): break
            _worker.jobs.task_done()
        except:
            # clean-up the queue
            while _worker.jobs.unfinished_tasks:
                try:
                    _worker.jobs.task_done()
                except ValueError:
                    pass # just to handle racing issues with other workers
            raise
_worker.jobs = Queue.Queue()

for unused_i in range(WORKERS):
    t = threading.Thread(target=_worker)
    t.daemon = True
    t.start()

def paste_image(target_image, im, left, top, mask):
    with paste_image.lock:
        try:
            target_image.paste(im, (left, top), mask)
            return True
        except ValueError:
            print "Ignored  file: " + str(im.filename)
            print "  Format: " + str(im.format)
            print "  Size: %d,%d" % im.size
            print "  Mode: " + str(im.mode)
            print "  Position: %d,%d" % (left, top)
            return False
paste_image.lock = threading.Lock()

def perform_loop(proc, amt):
    for i in range(amt):
        _worker.jobs.put([proc, i])
    _worker.jobs.join()
