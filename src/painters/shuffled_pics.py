# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import Image
import random

import pilext

from filters import addborder
from util_m import paste_image, perform_loop

def shuffled_pics(pic_stream, target_image, tilesize):
    """
    Randomly places "photos" (pictures with white border) on the target image,
    with several dispositions.
    """
    def paint_picture(_unused_i):
        im = pic_stream.get()
        im = addborder(im, external=True)
        mask = Image.new('L', im.size, 'white')
        angle = random.randint(-30, 30)
        im = pilext.anti_alias_rotate(im, angle)
        rmask = pilext.anti_alias_rotate(mask, angle)
        top = random.randint(-round(im.size[1] / 3),
                             target_image.size[1] - round(im.size[0] * 2 / 3))
        left = random.randint(-round(im.size[0] / 3),
                              target_image.size[0] - round(im.size[0] * 2 / 3))
        return paste_image(target_image, im, left, top, rmask)

    random.seed()
    # amount determined with some heuristic
    amount = (target_image.size[0] * target_image.size[1]) / (tilesize[0] * tilesize[1])
    perform_loop(paint_picture, amount)
    return amount
