# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from contextlib import contextmanager
import Queue
import collections
import os
import threading

import Image

import pilext

def default_short_side_length(value):
    """
    Sets the length of the shorter side of a picture.
    This value is being used to ensure all pictures are of the same size
    @param value: new length to use
    @return: old value of the length (used for testing)
    """
    assert value >= 0, 'Wrong lenght!'
    oldval = default_short_side_length.val
    default_short_side_length.val = value
    return oldval
default_short_side_length.val = None

class _PicStream(Queue.Queue):
    """
    A specialized queue:
        - put() a filename, get() an Image
        - get() is blocking, but may be aborted from another thread
    Note: canceling get() is not really in this class responsibility; however
    splitting into two classes may be a bit too draconian at this moment.
    """
    def __init__(self, maxsize):
        Queue.Queue.__init__(self, maxsize)
        self.aborted = False
    def get(self):
        while True:
            try:
                image_info = Queue.Queue.get(self, timeout=1)
                break
            except Queue.Empty:
                if self.aborted: raise
        im = Image.open(image_info.filename)
        if os.path.splitext(image_info.filename)[1] in ('jpg', 'jpeg'):
            im = pilext.exif_rotate(im)
        im = pilext.resize_fit(im, default_short_side_length.val)
        if image_info.delete:
            os.remove(image_info.filename)
        return im
    def abort(self):
        self.aborted = True

@contextmanager
def open(provider, *provider_params):
    """
    Wrapper for providing queue functionality
    @param provider: generator returning an ImageInfo tuple
    @param provider_params: parameters for the generator
    @return: queue (to be used in a with statement)
    """
    assert default_short_side_length.val is not None
    q = _PicStream(10)
    def fill_queue():
        try:
            while True:
                for image_info in provider(*provider_params):
                    q.put(image_info)
        except:
            q.abort()
            raise

    reading_thread = threading.Thread(target=fill_queue)
    reading_thread.daemon = True
    reading_thread.start()
    yield q

ImageInfo = collections.namedtuple('ImageInfo', 'filename, delete')
