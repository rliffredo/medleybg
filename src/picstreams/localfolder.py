# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import random

from picstreams.util import open, ImageInfo

def open_folder(foldername):
    """
    @return: a picture stream from the directory specified in the parameter
    """
    return open(_get_from_folder, foldername)


def _get_from_folder(folderPath):
    """
    Image list from local folder
    """
    assert(folderPath)
    assert(os.path.exists(folderPath))
    file_list = []
    for root, dummy_dirs, files in os.walk(folderPath):
        file_list += [(root, fileName)
                      for fileName in files
                      if fileName[-3:].lower() in ('jpg', 'png')]
    assert file_list, 'folder with images should not be empty [%s]' % folderPath
    random.shuffle(file_list)
    while True:
        for file_element in file_list:
            file_name = os.path.join(file_element[0], file_element[1])
            yield ImageInfo(filename=file_name, delete=False)
