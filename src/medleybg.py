# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import Image
import time
from contextlib import contextmanager
from optparse import OptionParser
import ctypes

import picstreams
import painters
import filters


def script(img, options):
    picstreams.default_short_side_length(options.tilesize)
    # paint grey background
    with picstreams.open_folder(options.directory) as fl:
        painters.tiled_pics(fl, img,
                            tgt_topleft=(0, 0),
                            tgt_size=img.size,
                            tilesize=(options.tilesize, options.tilesize),
                            debug=options.debug)
    img = filters.desaturate(img)
    # Paint other photos
    with picstreams.open_folder(options.directory) as fl:
        painters.shuffled_pics(fl, img,
                               tilesize=(options.tilesize, options.tilesize))
    return img

@contextmanager
def perfcounter():
    timestart = time.clock()
    try:
        print "Starting image medley..."
        yield
    finally:
        timeend = time.clock()
        totaltime = timeend - timestart
        print "Total time for processing: " + str(totaltime)

def main():
    user32 = ctypes.windll.user32
    screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename",
                      help="write medley image to FILE", metavar="FILE")
    parser.add_option("-w", "--width", dest="width", default=screensize[0],
                      help="width of medley image", type="int")
    parser.add_option("-e", "--height", dest="height", default=screensize[1],
                      help="height of medley image", type="int")
    parser.add_option("-t", "--tilesize", dest="tilesize", default=150,
                      help="size for smaller side of compounding images",
                      type="int")
    parser.add_option("-d", "--scandir", dest="directory",
                      help="directory containing source images")
    parser.add_option("--debug", dest="debug", action="store_true",
                      default=False, help="enable debug output")
    options, unused_args = parser.parse_args()
    if options.filename is None or options.directory is None:
        parser.print_help()
        return

    with perfcounter():
        bg = Image.new('RGB',
                                     (options.width, options.height))
        bg = script(bg, options)
        bg.save(options.filename, "JPEG")
    os.system(options.filename)

if __name__ == "__main__":
    main()
