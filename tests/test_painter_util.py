# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest

import time
import Image

import painters.util_m
import painters.util_s

class TestLoopBase(unittest.TestCase):
    """
    Base class for tests; it actually checks the single-threaded version of the
    code, but this behavior may be changed by altering the alias for tested
    module 
    """
    loop_util = painters.util_s

    def setUp(self):
        self.loops = []

    def test_paste_image(self):
        """
        Test a single paste; it will not check for possible multithreaded issues
        """
        img1 = Image.new('RGB', (100, 100), 'white')
        img2 = Image.new('RGB', (100, 100), 'black')
        self.assertNotEqual(img1.getcolors(), img2.getcolors(),
                            'Testing images should be different')
        self.loop_util.paste_image(img1, img2, 0, 0, None)
        self.assertEqual(img1.getcolors(), img2.getcolors(),
                         'Paste operation was not successful!')

    def test_perform_loop(self):
        def test_proc(iter_number):
            time.sleep(0.001) # a small sleep will expose multithread issues
            self.loops.append(iter_number)
            return True
        self.loop_util.perform_loop(test_proc, 10)
        # Make up for out-of order execution in multithreaded mode
        self.loops.sort()
        self.assertEqual(self.loops, range(10),
                         'Unexpected loop result: %s' % str(self.loops))

class TestMultiThread(TestLoopBase):
    """
    Specialization of test class, that will make it use the multi-threaded
    version
    """
    loop_util = painters.util_m

def suite():
    suite = unittest.TestSuite()
    suite.addTest(TestMultiThread())
    return suite

if __name__ == "__main__":
    unittest.main()
