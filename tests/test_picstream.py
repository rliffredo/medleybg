# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import os
from tempfile import NamedTemporaryFile, mkdtemp

import Image

import picstreams.localfolder
import picstreams.util

def _create_testfile(color, dir=None):
    im = Image.new('RGB', (50, 50), color)
    with NamedTemporaryFile('wb', suffix='.png', dir=dir, delete=False) as tf:
        im.save(tf, 'PNG')
    return tf.name

def _get_mock():
    """ Mock image list """
    # create two temporary files, white and black
    temp_file_black = _create_testfile('black')
    yield picstreams.util.ImageInfo(filename=temp_file_black, delete=True)
    temp_file_white = _create_testfile('white')
    yield picstreams.util.ImageInfo(filename=temp_file_white, delete=True)
    # just to have something to fill the remaining queue
    while True:
        yield picstreams.util.ImageInfo(filename='', delete=False)

class TestFolder(unittest.TestCase):

    def test__get_from_folder(self):
        temp_filename = temp_dirname = None
        try:
            # No dir
            stream = picstreams.localfolder._get_from_folder('c:\asdasdadasda')
            self.assertRaises(AssertionError, stream.next)
            # Empty dir
            temp_dirname = mkdtemp()
            stream = picstreams.localfolder._get_from_folder(temp_dirname)
            self.assertRaises(AssertionError, stream.next)
            # Non-empty dir
            temp_filename = _create_testfile('white', temp_dirname)
            stream = picstreams.localfolder._get_from_folder(temp_dirname)
            iminfo = stream.next()
            self.assertEqual(iminfo.filename, temp_filename)
            self.assertFalse(iminfo.delete, 'Expected read-only file')
        finally:
            if temp_filename and os.path.exists(temp_filename):
                os.remove(temp_filename)
            if temp_dirname and os.path.exists(temp_dirname):
                os.rmdir(temp_dirname)

class TestUtil(unittest.TestCase):

    def test_short_side_lenght(self):
        picstreams.util.default_short_side_length(100)
        olddflt = picstreams.util.default_short_side_length(10)
        self.assertEqual(olddflt, 100)
        try:
            picstreams.util.default_short_side_length(-1)
            did_assert = False
        except AssertionError:
            did_assert = True
        self.assertTrue(did_assert,
                        "Was able to set invalid value on short_side_lenght")

    def test_open(self):
        picstreams.util.default_short_side_length(100)
        with picstreams.util.open(_get_mock) as picgen:
            iminfo = picgen.get()
            self.assertEqual(iminfo.getcolors()[0], (10000, (0, 0, 0)))
            iminfo = picgen.get()
            self.assertEqual(iminfo.getcolors()[0], (10000, (255, 255, 255)))

if __name__ == "__main__":
    unittest.main()
