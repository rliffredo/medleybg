# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import Image
from filters import desaturate, addborder, putnumber


class TestDesaturate(unittest.TestCase):

    def test_desaturate(self):
        im = Image.new('RGB', (100, 100), 'rgb(100, 100, 100)')
        im2 = desaturate(im)
        self.assertTrue(im2.getcolors()[0] == (10000, (50, 50, 50)),
                        'Wrong desaturation')

class TestBorder(unittest.TestCase):

    def setUp(self):
        self.im = Image.new('RGB', (100, 100), 'black')

    def test_border_external(self):
        img = addborder(self.im, external=True)
        new_size = (self.im.size[0] + 20, self.im.size[1] + 20)
        self.assertEqual(img.size, new_size)
        colors = img.getcolors()
        colors.sort()
        self.assertTrue(colors == [(4400, (255, 255, 255)), (10000, (0, 0, 0))],
                        'Wrong colors - got %s' % str(colors))

    def test_border_internal(self):
        img = addborder(self.im, external=False)
        self.assertEqual(img.size, self.im.size)
        colors = img.getcolors()
        colors.sort()
        self.assertTrue(colors == [(3600, (255, 255, 255)), (6400, (0, 0, 0))],
                        'Wrong colors - got %s' % str(colors))

class TestNumber(unittest.TestCase):
    def test_putnumber(self):
        img = Image.new('RGB', (100, 100), 'black')
        img2 = putnumber(img, 1)
        self.assertTrue(img.histogram() != img2.histogram())
        self.assertTrue((255, 0, 255) in [col[1] for col in img2.getcolors()])

if __name__ == "__main__":
    unittest.main()
