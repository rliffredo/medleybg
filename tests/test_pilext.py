# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest

import Image
import pilext

class Test(unittest.TestCase):

    def test_anti_alias_rotate(self):
        imtest = Image.new('L', (10, 1))
        rotated = pilext.anti_alias_rotate(imtest, 0)
        self.assertEqual(rotated.tostring(), imtest.tostring())
        rotated = pilext.anti_alias_rotate(imtest, 30)
        self.assertEqual(rotated.size, (9, 6))
        rotated = pilext.anti_alias_rotate(imtest, 45)
        self.assertEqual(rotated.size, (8, 8))
        rotated = pilext.anti_alias_rotate(imtest, 90)
        self.assertEqual(rotated.size, (1, 10))

    def test_exif_rotate(self):
        imtest = Image.new('RGB', (10, 1))
        # First test: image without exif attributes
        exif_rotated = pilext.exif_rotate(imtest)
        self.assertEqual(imtest.tostring(), exif_rotated.tostring())
        # Second test: image with exif attributes
        imtest._getexif = lambda: [8 for unused_i in range(275)]
        rotated = imtest.rotate(90, Image.NEAREST, True)
        exif_rotated = pilext.exif_rotate(imtest)
        self.assertEqual(rotated.tostring(), exif_rotated.tostring())

    def test_resize_fit(self):
        imtest = Image.new('L', (20, 10))
        resized = pilext.resize_fit(imtest, 10)
        self.assertEqual(resized.size, (20, 10))
        resized = pilext.resize_fit(imtest, 5)
        self.assertEqual(resized.size, (10, 5))
        resized = pilext.resize_fit(imtest, 20)
        self.assertEqual(resized.size, (40, 20))
        imtest = Image.new('L', (10, 20))
        resized = pilext.resize_fit(imtest, 30)
        self.assertEqual(resized.size, (30, 60))

if __name__ == "__main__":
    unittest.main()
