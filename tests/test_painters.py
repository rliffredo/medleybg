# Copyright (c) 2010 Roberto Liffredo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import Image

from painters.shuffled_pics import shuffled_pics
from painters.tiled_pics import tiled_pics, _get_coords, _get_amt


class mock_picstream(object):
    im = Image.new('RGB', (20, 20), 'white')
    def get(self):
        return self.im

class TestTiledPicsSupport(unittest.TestCase):

    def test__get_amt(self):
        self.assertEqual(_get_amt((10, 10), (0, 0)), 0)
        self.assertEqual(_get_amt((10, 10), (100, 100)), 100)
        self.assertEqual(_get_amt((10000000000000, 10000000000000),
                                  (100000000000000, 100000000000000)), 100)
        self.assertEqual(_get_amt((10, 11), (100, 100)), 100)
        self.assertEqual(_get_amt((11, 10), (100, 100)), 100)
        self.assertEqual(_get_amt((11, 11), (100, 100)), 100)
        self.assertEqual(_get_amt((10, 9), (100, 100)), 120)
        self.assertEqual(_get_amt((10, 9), (100, 99)), 110)
        self.assertEqual(_get_amt((10, 10), (100, 200)), 200)
        self.assertEqual(_get_amt((10, 10), (100, 199)), 200)
        self.assertEqual(_get_amt((3, 3), (10, 10)), 16)

    def test__get_coords(self):
        # horz / vert
        def get_coord_square(n):
            return _get_coords(n, (10, 10), (100, 100))
        self.assertEqual(get_coord_square(0), (0, 0))
        self.assertEqual(get_coord_square(1), (10, 0))
        self.assertEqual(get_coord_square(2), (20, 0))
        self.assertEqual(get_coord_square(9), (90, 0))
        self.assertEqual(get_coord_square(10), (0, 10))
        self.assertEqual(get_coord_square(11), (10, 10))
        self.assertEqual(get_coord_square(19), (90, 10))
        self.assertEqual(get_coord_square(20), (0, 20))
        self.assertEqual(get_coord_square(99), (90, 90))
        def get_coord_rect1(n):
            return _get_coords(n, (10, 10), (200, 100))
        self.assertEqual(get_coord_rect1(0), (0, 0))
        self.assertEqual(get_coord_rect1(1), (10, 0))
        self.assertEqual(get_coord_rect1(2), (20, 0))
        self.assertEqual(get_coord_rect1(9), (90, 0))
        self.assertEqual(get_coord_rect1(10), (100, 0))
        self.assertEqual(get_coord_rect1(20), (0, 10))
        self.assertEqual(get_coord_rect1(21), (10, 10))
        def get_coord_rect2(n):
            return _get_coords(n, (10, 10), (100, 200))
        self.assertEqual(get_coord_rect2(0), (0, 0))
        self.assertEqual(get_coord_rect2(1), (10, 0))
        self.assertEqual(get_coord_rect2(2), (20, 0))
        self.assertEqual(get_coord_rect2(9), (90, 0))
        self.assertEqual(get_coord_rect2(10), (0, 10))
        self.assertEqual(get_coord_rect2(20), (0, 20))
        self.assertEqual(get_coord_rect2(21), (10, 20))
        def get_coord_rect3(n):
            return _get_coords(n, (20, 10), (100, 100))
        self.assertEqual(get_coord_rect3(0), (0, 0))
        self.assertEqual(get_coord_rect3(1), (20, 0))
        self.assertEqual(get_coord_rect3(4), (80, 0))
        self.assertEqual(get_coord_rect3(5), (0, 10))
        self.assertEqual(get_coord_rect3(6), (20, 10))
        self.assertEqual(get_coord_rect3(10), (0, 20))
        self.assertEqual(get_coord_rect3(11), (20, 20))
        def get_coord_rect4(n):
            return _get_coords(n, (3, 2), (11, 7))
        self.assertEqual(get_coord_rect4(0), (0, 0))
        self.assertEqual(get_coord_rect4(1), (3, 0))
        self.assertEqual(get_coord_rect4(3), (9, 0))
        self.assertEqual(get_coord_rect4(4), (0, 2))
        def get_coord_primes(n):
            return _get_coords(n, (3, 3), (10, 10))
        self.assertEqual(get_coord_primes(9), (3, 6))
        self.assertEqual(get_coord_primes(10), (6, 6))
        self.assertEqual(get_coord_primes(11), (9, 6))

class TestTiledPics(unittest.TestCase):

    def setUp(self):
        self.img = Image.new('RGB', (100, 100), 'black')
        self.q = mock_picstream()

    def tearDown(self):
        f = open('c:/temp/n.bmp', 'w')
        self.img.save(f)
        f.close()

    def test_square(self):
        pasted = tiled_pics(self.q, self.img, (0, 0), (100, 100), (20, 20))
        self.assertEqual(self.img.histogram()[255], 10000)
        self.assertEqual(pasted, 25)

    def test_rectangle_fit(self):
        pasted = tiled_pics(self.q, self.img, (0, 0), (100, 60), (20, 20))
        self.assertEqual(self.img.histogram()[255], 6000)
        self.assertEqual(pasted, 15)

    def test_rectangle_notfit(self):
        pasted = tiled_pics(self.q, self.img, (0, 0), (100, 50), (20, 20))
        self.assertEqual(self.img.histogram()[255], 6000)
        self.assertEqual(pasted, 15)

class TestShuffledPics(unittest.TestCase):

    def test_shuffled_pics(self):
        """
        Insert two white images on a black background.
        Only assertion is that at least one pixel has been covered; this is
        quite a weak assertion.
        """
        im = Image.new('RGB', (100, 100))
        q = mock_picstream()
        pasted = shuffled_pics(q, im, (50, 50))
        self.failUnless(im.histogram()[0] < 10000,
                        'There should have been at least one white pixel')
        self.assertEqual(pasted, 4)

if __name__ == "__main__":
    unittest.main()
